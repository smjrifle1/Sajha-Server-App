package com.kazi.sajha;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
 
public class LoginActivity extends Activity {
     
    // Email, password edittext
    EditText vehicleRegNumber,VehicleRoute,vehicleLotNumber;
    // login button
    Button btnLogin;
    // Session Manager Class
    SessionManager session;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.vehicle_reg);
         
        // Session Manager
        session = new SessionManager(getApplicationContext());               
         
        // Email, Password input text
       vehicleLotNumber = (EditText) findViewById(R.id.vehLot);
       vehicleRegNumber = (EditText) findViewById(R.id.vehNum);
       VehicleRoute = (EditText) findViewById(R.id.vehRoute);

         
        // Login button
        btnLogin = (Button) findViewById(R.id.loginbtn);
         
         
        // Login button click event
        btnLogin.setOnClickListener(new View.OnClickListener() {
             
            @Override
            public void onClick(View arg0) {
                // Get vehicleName, password from EditText
            	 
            	if( 	vehicleLotNumber.getText().toString() == " " ||  // Check if all the fields are filled
            			vehicleRegNumber.getText().toString() == " " || 
            			VehicleRoute.getText().toString() == " "
            			) 
            		{ Toast.makeText(getApplicationContext(), "Please enter valid data", Toast.LENGTH_SHORT); return; }
            	
				String vehicleName = "ba" + vehicleLotNumber.getText().toString() + "kha" + 
                					vehicleRegNumber.getText().toString();
                String route = VehicleRoute.getText().toString();
                
                vehicleName = vehicleName.toLowerCase();
                route = route.toLowerCase();
                
                 
                // Check if vehicleName, password is filled               
                if(vehicleName.trim().length()  > 0 &&  route.trim().length()  > 0){
                    // For testing puspose vehicleName, password is checked with sample data
                    // vehicleName = test
                    // password = test
                    if(vehicleName.equals("kazi") && route.equals("test")){ // for test purpose
                         
                        // Creating user login session
                        // For testing i am stroing name, email as follow
                        // Use user real data
                    	session.createLoginSession("ba1kha1234","test"); 
                    }
                    
                    else{
                        // add new user  - 
                    	/*
                    	 * find a way to verify the entered user name
                    	 */
                    	session.createLoginSession(vehicleName,route); 
                      
                    } 
                        // Staring MainActivity
                        Intent i = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(i);
                        finish();       
                }
                else
                {
                    // user didn't entered vehicleName or password
                    // Show alert asking him to enter the details
                    Toast.makeText(getApplicationContext(), "Please enter vehicle route and registration number", Toast.LENGTH_SHORT).show();  
                }
                 
            }
        });
        }
    
    
	   @Override
	    protected void onResume() {
		  
	        super.onResume();

	
	    }
	   @Override
	   public void onBackPressed() {
		   Intent intent = new Intent(getApplicationContext(), MainActivity.class);
           intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
           intent.putExtra("EXIT", true);
           startActivity(intent);
	   }
	    @Override
	    protected void onPause() {

	        
	        super.onPause();
	    }
}