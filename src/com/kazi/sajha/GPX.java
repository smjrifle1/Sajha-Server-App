package com.kazi.sajha;

public class GPX {
	private String message;
	private String latitude;
	private String lontitude;
	private String device_ID;
	private String time_stamp;
	private String speed;
	private String accuracy;
	private String route;
	private String android_id;
	private String battery_level;
	private String internet_balance;

	public String getLat() {
		return this.latitude;
	}

	public String getStatus() {
		return this.message;
	}

	public String getAndroidID() {
		return this.android_id;
	}

	public String getBattLevel() {
		return this.battery_level;
	}

	public String getInternetBalance() {
		return this.internet_balance;
	}

	public String getRoute() {
		return this.route;
	}

	public String getLon() {
		return this.lontitude;
	}

	public String getAccuracy() {
		return this.accuracy;
	}

	public String getDeviceID() {
		return this.device_ID;
	}

	public String getTimeStamp() {
		return this.time_stamp;
	}

	public String getSpeed() {
		return this.speed;
	}

	public void setLat(String value) {
		this.latitude = value;

	}
	public void setStatus(String value){
		this.message = value;
		
	}

	public void setLon(String value) {
		this.lontitude = value;

	}

	public void setRoute(String value) {
		this.route = value;
	}

	public void setTimeStamp(String string) {
		this.time_stamp = string;

	}

	public void setDeviceID(String string) {
		this.device_ID = string;

	}

	public void setSpeed(String value) {
		this.speed = value;

	}

	public void setAccuracy(String value) {
		this.accuracy = value;

	}

	public void setBattLevel(String value) {
		this.battery_level = value;

	}

	public void setAndroidID(String value) {
		this.android_id = value;

	}

	public void setInternetBalance(String value) {
		this.internet_balance = value;

	}

}
